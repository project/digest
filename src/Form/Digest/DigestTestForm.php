<?php

namespace Drupal\digest\Form\Digest;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends out a test digest to a given user.
 *
 * @see \Drupal\digest\Entity\Digest
 */
class DigestTestForm extends EntityForm {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {

    $instance = parent::create($container);

    $instance->currentUser = $container->get('current_user');

    return $instance;

  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $user_storage = $this->entityTypeManager->getStorage('user');

    $form['send_to_user'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#title' => $this->t('Send to user'),
      '#description' => $this->t('The digest will be sent out to this user.'),
      '#default_value' => $user_storage->load($this->currentUser->id()),
      '#selection_settings' => [
        'include_anonymous' => FALSE,
      ],
    ];

    return $form;

  }

  /**
   * {@inheritDoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {

    // Rename the submit button to 'send' and remove the save callback.
    return [
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Send'),
        '#submit' => [
          '::submitForm',
        ],
      ],
    ];

  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);

    try {

      $user_storage = $this->entityTypeManager->getStorage('user');

      /** @var \Drupal\user\UserInterface $user */
      $user = $user_storage->load($form_state->getValue('send_to_user'));

      $this->entity->send($user);

    }
    catch (EntityMalformedException $exception) {

      $this->messenger()->addError($this->t('The digest could not be sent.'));
      return;

    }

    $this->messenger()->addStatus($this->t('The digest was sent successfully.'));

  }

}
