<?php

namespace Drupal\digest\Form\Digest;

use Cron\CronExpression;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Lorisleiva\CronTranslator\CronTranslator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for editing and adding digest entities.
 *
 * @see \Drupal\digest\Entity\Digest
 */
class DigestEditForm extends EntityForm {

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {

    $instance = parent::create($container);

    $instance->blockManager = $container->get('plugin.manager.block');
    $instance->contextRepository = $container->get('context.repository');
    $instance->themeManager = $container->get('theme.manager');

    return $instance;

  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $is_new = $this->entity->isNew();

    $form['settings_header'] = [
      '#markup' => '<h2>' . $this->t('Settings') . '</h2>',
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#disabled' => !$is_new,
      '#machine_name' => [
        'exists' => '\Drupal\digest\Entity\Digest::load',
      ],
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Send out notifications for this digest?'),
      '#default_value' => $this->entity->get('status'),
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Digest Title'),
      '#description' => $this->t('This is used in the subscription form for digests.'),
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Digest Description'),
      '#description' => $this->t('This is used in the subscription form for digests.'),
      '#default_value' => $this->entity->get('description'),
    ];

    $form['schedule_field_set'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('When to send'),
    ];

    // ID used to specify the out put for the cron translation callback.
    $schedule_wrapper_id = 'digest-schedule-output';
    $schedule_value = $this->entity->get('schedule');

    $form['schedule_field_set']['schedule'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cron expression'),
      '#description' => $this->t('A cron expression that denotes when this
        digest is sent out. To read about and create cron expressions visit
        <a href="@crontab">Crontab.guru</a>.', [
          '@crontab' => 'https://crontab.guru/',
        ]),
      '#default_value' => $schedule_value,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::updateCronTranslation',
        'wrapper' => $schedule_wrapper_id,
        'event' => 'change',
        'disable-refocus' => FALSE,
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Translating cron...'),
        ],
      ],
    ];

    $form['schedule_field_set']['schedule_output'] = [
      '#prefix' => '<div id="' . $schedule_wrapper_id . '"><p>',
      '#suffix' => '</p></div>',
      '#plain_text' => isset($schedule_value) ? CronTranslator::translate($schedule_value) : '',
    ];

    $form['display_header'] = [
      '#markup' => '<h2>' . $this->t('Display') . '</h2>',
    ];

    $form['display_block'] = [
      '#type' => 'select',
      '#title' => $this->t('Block to display'),
      '#description' => $this->t('This is the block that will be displayed as
        the body of this digest.'),
      '#default_value' => $this->entity->get('display_block'),
      '#required' => TRUE,
    ];

    // Get all block plugins.
    $definitions = $this->blockManager
      ->getFilteredDefinitions(
        'block_ui',
        $this->contextRepository->getAvailableContexts(),
        ['theme' => $this->themeManager->getActiveTheme()->getName()],
      );

    // Filter out definitions that are not intended to be placed by the UI.
    $definitions = array_filter($definitions, function (array $definition) {
      return empty($definition['_block_ui_hidden']);
    });

    // Add each block to the options of which to display.
    foreach ($definitions as $id => $definition) {

      $form['display_block']['#options'][$id] = $definition['admin_label'];

    }

    return $form;

  }

  /**
   * Updates the cron translation for the schedule field.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   A render array.
   */
  public static function updateCronTranslation(array $form, FormStateInterface $form_state) {

    // Only valid expressions are passed in since validation runs before this.
    $translation = CronTranslator::translate($form_state->getValue('schedule'));

    $form['schedule_field_set']['schedule_output']['#plain_text'] = $translation;

    return $form['schedule_field_set']['schedule_output'];

  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    parent::validateForm($form, $form_state);

    $schedule = $form_state->getValue('schedule');

    // Ensure the schedule is valid cron.
    try {

      new CronExpression($schedule);

    }
    catch (\InvalidArgumentException $exception) {

      $form_state->setErrorByName('schedule', $exception->getMessage());

    }

  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $status = $this->entity->save();

    if ($status === SAVED_NEW) {

      $this->messenger()->addMessage($this->t('The @name digest has been created',
        ['@name' => $this->entity->label()]));

    }
    else {

      $this->messenger()->addMessage($this->t('The @name digest has been updated',
        ['@name' => $this->entity->label()]));

    }

    $form_state->setRedirectUrl(Url::fromRoute('entity.digest.digest_display'));

  }

}
