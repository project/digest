<?php

namespace Drupal\digest\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The main settings form for the Digest module.
 */
class DigestSettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'digest_settings';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {

    return [
      'digest.settings',
    ];

  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('digest.settings');

    $form['send_from_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Digest from email'),
      '#description' => $this->t('The email to use when sending digests.'),
      '#default_value' => $config->get('digest_from_email'),
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('digest.settings');

    $config->set('digest_from_email', $form_state->getValue('send_from_email'));

    $config->save();

    parent::submitForm($form, $form_state);

  }

}
