<?php

namespace Drupal\digest;

/**
 * Defines the functionality for a digest manager.
 *
 * This allows for easy interactions over digest entities.
 */
interface DigestManagerInterface {

  /**
   * Returns an array of all enabled digests.
   *
   * @return \Drupal\digest\Entity\DigestInterface[]
   *   All enabled digests.
   */
  public function getEnabled();

  /**
   * Gets digests for use in an options form element.
   *
   * Should return an array with all active digests keyed like:
   * 'Digest ID => Digest Title'.
   *
   * @return array
   *   An array with digests laid out as options.
   */
  public function getAsOptions();

  /**
   * Queues up any digests that need to be sent out to users.
   *
   * If a digest is queued up, its send dates should be updated as well.
   */
  public function queueAll();

  /**
   * Prepares a message from digests for mailing.
   *
   * This should only be called with data from a digest that is sending out it's
   * mail.
   *
   * No extra rendering or content is inserted at this step. Merely prepares
   * the data given.
   *
   * @param string $key
   *   The mail identifier.
   * @param array $message
   *   The message to prepare.
   *   See 'hook_mail' for more details.
   * @param array $params
   *   The parameters passed from the digest.
   *   Expected to have the keys :
   *     - 'subject', the subject of the email.
   *     - 'body', the body of the email.
   *
   * @return array
   *   The prepared message.
   *
   * @see \hook_mail()
   * @see \Drupal\digest\Entity\DigestInterface::send()
   */
  public function prepareMail($key, array $message, array $params);

}
