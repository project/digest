<?php

namespace Drupal\digest\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\digest\Entity\DigestInterface;

/**
 * A controller for actions with digests.
 */
class DigestController extends ControllerBase {

  /**
   * Performs a given operation on a digest entity.
   *
   * @param \Drupal\digest\Entity\DigestInterface $digest
   *   The digest to do the operation on.
   * @param string $operation
   *   The operation to perform.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect, usually back to the digest listing page.
   *
   * @throws \BadMethodCallException
   *   Thrown when $operation does not exist on the digest.
   */
  public function performOperation(DigestInterface $digest, string $operation) {

    // Ensure the method exists before trying to call it.
    if (!method_exists($digest, $operation)) {
      throw new \BadMethodCallException('The method "' . $operation . '" does not exist for digests.');
    }

    $digest->$operation()->save();

    return $this->redirect('entity.digest.digest_display');

  }

}
