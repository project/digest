<?php

namespace Drupal\digest\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\RequeueException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The queue worker that is responsible for sending out digests.
 *
 * Data passed into this queue should be an array with the following keys:
 *   - 'user', the ID of the user to send to.
 *   - 'digest', the ID of the digest that should be sent.
 *
 * @QueueWorker(
 *   id = "digest_send",
 *   title = @Translation("Digest Send Queue"),
 *   cron = {
 *     "time" = 30,
 *   },
 * )
 */
class DigestSendQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The digest storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $digestStorage;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    $instance = new static($configuration, $plugin_id, $plugin_definition);

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $container->get('entity_type.manager');

    $instance->userStorage = $entityTypeManager->getStorage('user');
    $instance->digestStorage = $entityTypeManager->getStorage('digest');

    return $instance;

  }

  /**
   * {@inheritDoc}
   */
  public function processItem($data) {

    /** @var \Drupal\user\UserInterface $user */
    $user = $this->userStorage->load($data['user']);

    /** @var \Drupal\digest\Entity\DigestInterface $digest */
    $digest = $this->digestStorage->load($data['digest']);

    // Send out the digest to the given user.
    try {

      $digest->send($user);

    }
    catch (EntityMalformedException $exception) {

      throw new RequeueException(
        $digest->id() . ' could not be sent to user ' . $user->id() . '.',
        0, $exception);

    }

  }

}
