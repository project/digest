<?php

namespace Drupal\digest\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A widget that displays and saves data for users subscriptions to digests.
 *
 * @FieldWidget(
 *   id = "digest_subscription_widget",
 *   label = @Translation("Digest subscriptions"),
 *   description = @Translation("Displays and allows subscription to digests."),
 *   module = "digest",
 *   field_types = {
 *     "digest_subscription",
 *   },
 *   multiple_values = TRUE
 * )
 */
class SubscriptionWidget extends OptionsWidgetBase {

  /**
   * The digest entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $digestStorage;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->digestStorage = $container->get('entity_type.manager')->getStorage('digest');

    return $instance;

  }

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element += [
      '#type' => 'checkboxes',
      '#options' => [],
    ];

    $options = $this->getOptions($items->getEntity());

    // Return default text if no digests are enabled.
    if (empty($options)) {

      $element['#markup'] = $this->t('There are currently no digests available.');
      return $element;

    }

    $element['#options'] = $options;
    $element['#default_value'] = $this->getSelectedOptions($items);
    $element += $this->getValueData($items->getEntity());

    return $element;

  }

  /**
   * Returns an array to describe the options given by ::getOptions.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity for which to return options.
   *
   * @return array
   *   The array of data to add on to options of the widget.
   *
   * @todo Add data for showing when the digest is sent out.
   */
  protected function getValueData(FieldableEntityInterface $entity) {

    $values = [];

    foreach ($this->getOptions($entity) as $key => $value) {

      $values += [
        $key => [
          '#description' => $this->digestStorage->load($key)->getDescription(),
        ],
      ];

    }

    return $values;

  }

}
