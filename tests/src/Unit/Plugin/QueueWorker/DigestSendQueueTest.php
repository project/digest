<?php

namespace Drupal\Tests\digest\Kernel\Plugin\QueueWorker;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\RequeueException;
use Drupal\digest\Entity\DigestInterface;
use Drupal\digest\Plugin\QueueWorker\DigestSendQueue;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;

/**
 * Tests the digest send queue worker.
 *
 * @group digest
 *
 * @coversDefaultClass \Drupal\digest\Plugin\QueueWorker\DigestSendQueue
 *
 * @see \Drupal\digest\Plugin\QueueWorker\DigestSendQueue
 */
class DigestSendQueueTest extends UnitTestCase {

  /**
   * The digest queue worker.
   *
   * @var \Drupal\digest\Plugin\QueueWorker\DigestSendQueue
   */
  protected $queueWorker;

  /**
   * A mocked user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * A container for our mocked services.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * A mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A mocked user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * A mocked digest storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $digestStorage;

  /**
   * A mocked mal-formed digest.
   *
   * @var \Drupal\digest\Entity\DigestInterface
   */
  protected $badDigest;

  /**
   * A mocked well formed digest.
   *
   * @var \Drupal\digest\Entity\DigestInterface
   */
  protected $goodDigest;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {

    parent::setUp();

    // Set up the mocked user.
    $this->user = $this->createMock(UserInterface::class);
    $this->user->expects($this->any())
      ->method('id')
      ->willReturn('1');

    // For simplicity, the user storage will always return the same user.
    $this->userStorage = $this->createMock(UserStorageInterface::class);
    $this->userStorage->expects($this->any())
      ->method('load')
      ->willReturn($this->user);

    // A poorly formed digest must throw an exception when sending.
    // We do not expect the 'send' function to be called more than once.
    $this->badDigest = $this->createMock(DigestInterface::class);
    $this->badDigest->expects($this->any())
      ->method('id')
      ->willReturn('bad_digest');
    $this->badDigest->expects($this->once())
      ->method('send')
      ->willThrowException(new EntityMalformedException('Digest was malformed.'));

    // For a properly formed digest we just need to verify that send was called.
    $this->goodDigest = $this->createMock(DigestInterface::class);
    $this->goodDigest->expects($this->any())
      ->method('id')
      ->willReturn('good_digest');
    $this->goodDigest->expects($this->atLeastOnce())
      ->method('send');

    // Set up the digest storage to load our digests.
    $this->digestStorage = $this->createMock(ConfigEntityStorageInterface::class);
    $this->digestStorage->expects($this->any())
      ->method('load')
      ->willReturnMap([
        [$this->badDigest->id(), $this->badDigest],
        [$this->goodDigest->id(), $this->goodDigest],
      ]);

    // Set up a mocked entity type manager.
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->entityTypeManager->expects($this->any())
      ->method('getStorage')
      ->willReturnMap([
        ['user', $this->userStorage],
        ['digest', $this->digestStorage],
      ]);

    // Set up the new container.
    $this->container = new ContainerBuilder();
    $this->container->set('entity_type.manager', $this->entityTypeManager);

    // Create the digest queue worker.
    $this->queueWorker = DigestSendQueue::create($this->container, [], 'digest_send', []);

  }

  /**
   * Tests the processing of items.
   *
   * The queue worker assumes all digests are in a sendable condition. Errors
   * that occur will be blamed on the user or the mail system.
   *
   * @covers ::processItem
   */
  public function testProcessItem() {

    // Create data for the queue worker.
    $data = [
      'digest' => $this->badDigest->id(),
      'user' => $this->user->id(),
    ];

    // If an error occurs the item should be released back into the queue.
    try {

      $this->queueWorker->processItem($data);
      $this->fail('No exception thrown for mal-formed digest being processed.');

    }
    catch (RequeueException $exception) {

      $this->assertStringContainsString($this->badDigest->id(), $exception->getMessage());

    }

    // Process a properly formed digest.
    // No exception should be thrown and the good digest will verify that it's
    // 'send' function was called.
    $data['digest'] = $this->goodDigest->id();
    $this->queueWorker->processItem($data);

  }

}
