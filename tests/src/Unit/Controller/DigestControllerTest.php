<?php

namespace Drupal\Tests\digest\Unit\Controller;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Routing\UrlGenerator;
use Drupal\digest\Controller\DigestController;
use Drupal\digest\Entity\Digest;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for the digest controller class.
 *
 * @group digest
 *
 * @coversDefaultClass \Drupal\digest\Controller\DigestController
 *
 * @see \Drupal\digest\Controller\DigestController
 */
class DigestControllerTest extends UnitTestCase {

  /**
   * The digest controller.
   *
   * @var \Drupal\digest\Controller\DigestController
   */
  protected $controller;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->controller = new DigestController();

  }

  /**
   * Tests applying operations to a digest.
   *
   * @covers ::performOperation
   */
  public function testPerformOperation() {

    // Create a mock digest.
    $digest = $this->createMock(Digest::class);
    $digest->expects($this->atLeastOnce())
      ->method('enable')
      ->willReturnSelf();
    $digest->expects($this->atLeastOnce())
      ->method('disable')
      ->willReturnSelf();
    $digest->expects($this->any())
      ->method('save');

    // Mock the URl generator.
    $digest_collection_link = '/admin/structure/digests';
    $url_generator = $this->createMock(UrlGenerator::class);
    $url_generator->expects($this->any())
      ->method('generateFromRoute')
      ->with('entity.digest.digest_display')
      ->willReturn($digest_collection_link);

    // Set up a container.
    $container = new ContainerBuilder();
    $container->set('url_generator', $url_generator);
    \Drupal::setContainer($container);

    // Test calling common operations on the digest.
    $operation = 'enable';

    $redirect = $this->controller->performOperation($digest, $operation);
    $this->assertStringContainsString($digest_collection_link, $redirect->getTargetUrl());

    $operation = 'disable';
    $redirect = $this->controller->performOperation($digest, $operation);
    $this->assertStringContainsString($digest_collection_link, $redirect->getTargetUrl());

    // Call a non-existent operation.
    try {

      $operation = 'Not an operation';
      $this->controller->performOperation($digest, $operation);
      $this->fail('No exception thrown for invalid operation');

    }
    catch (\BadMethodCallException $exception) {

      $this->assertStringContainsString($operation, $exception->getMessage());

    }

  }

}
