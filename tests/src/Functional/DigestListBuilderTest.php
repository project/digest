<?php

namespace Drupal\Tests\digest\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the DigestListBuilder class.
 *
 * @group digest
 *
 * @see \Drupal\digest\DigestListBuilder
 */
class DigestListBuilderTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'digest',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  private $adminUser;

  /**
   * {@inheritDoc}
   */
  public function setUp() : void {

    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
      'administer digest settings',
    ]);

  }

  /**
   * Tests the display of the DigestListBuilder when no digests are created.
   *
   * @covers \Drupal\digest\DigestListBuilder::buildHeader
   * @covers \Drupal\digest\DigestListBuilder::render
   */
  public function testDigestListBuilderEmptyDisplay() {

    $this->drupalLogin($this->adminUser);

    // Load the list builder.
    $this->drupalGet(Url::fromRoute('entity.digest.digest_display'));
    $this->assertSession()->statusCodeEquals(200);

    // Check that both status sections were generated.
    $this->assertSession()->pageTextContains('Enabled');
    $this->assertSession()->pageTextContains('Disabled');

    // Check that the header was generated.
    $this->assertSession()->pageTextContains('Title');
    $this->assertSession()->pageTextContains('Machine name');
    $this->assertSession()->pageTextContains('Description');
    $this->assertSession()->pageTextContains('Operations');

    // Check that the tables were generated.
    $this->assertSession()->pageTextContains('There are no enabled digests.');
    $this->assertSession()->pageTextContains('There are no disabled digests.');

    $this->assertSession()->pageTextNotContains('Edit');

  }

  /**
   * Tests the display of the DigestListBuilder when there are digests.
   *
   * @covers \Drupal\digest\DigestListBuilder::buildRow
   */
  public function testDigestListBuilderDisplay() {

    $this->drupalLogin($this->adminUser);

    // Create dummy digests.
    $digest_storage = \Drupal::entityTypeManager()->getStorage('digest');
    $digest_one = $digest_storage->create([
      'id' => 'viewOne',
      'title' => $this->randomMachineName(),
      'description' => $this->randomString(24),
    ]);
    $digest_one->save();
    $digest_two = $digest_storage->create([
      'id' => 'viewTwo',
      'status' => FALSE,
      'title' => $this->randomMachineName(),
      'description' => $this->randomString(24),
    ]);
    $digest_two->save();

    // Load the list builder.
    $this->drupalGet(Url::fromRoute('entity.digest.digest_display'));
    $this->assertSession()->statusCodeEquals(200);

    // Check digests were loaded.
    $this->assertSession()->pageTextNotContains('There are no enabled digests.');
    $this->assertSession()->pageTextNotContains('There are no disabled digests.');

    $this->assertSession()->pageTextContainsOnce($digest_one->get('title'));
    $this->assertSession()->pageTextContainsOnce($digest_one->get('description'));
    $this->assertSession()->pageTextContainsOnce($digest_two->get('title'));

    // Check that all operations are correctly loaded.
    $this->assertSession()->pageTextContains('test');
    $this->assertSession()
      ->linkByHrefExists($digest_one->toUrl('test-form')->toString());

    $this->assertSession()
      ->linkByHrefExists($digest_one->toUrl('disable')->toString());

    $this->assertSession()
      ->linkByHrefExists($digest_two->toUrl('enable')->toString());

  }

}
