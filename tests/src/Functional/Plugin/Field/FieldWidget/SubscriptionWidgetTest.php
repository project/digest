<?php

namespace Drupal\Tests\digest\Functional\Plugin\Field\FieldWidget;

use Drupal\digest\Entity\Digest;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the subscription widget.
 *
 * @group digest
 *
 * @see \Drupal\digest\Plugin\Field\FieldWidget\SubscriptionWidget
 */
class SubscriptionWidgetTest extends BrowserTestBase {

  /**
   * A testing user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'digest',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->user = $this->drupalCreateUser();
    $this->drupalLogin($this->user);

    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display */
    $form_display = \Drupal::service('entity_display.repository')
      ->getFormDisplay('user', 'user');

    // Attach the field to users.
    $form_display->setComponent('field_digest_subscriptions', [
      'type' => 'digest_subscription_widget',
      'region' => 'content',
    ])->save();

  }

  /**
   * Tests the correct text is displayed when no digests are enabled.
   */
  public function testEmptyWidget() {

    // Make sure disabled widgets aren't shown.
    Digest::create([
      'id' => 'my_digest',
      'title' => 'My digest',
      'status' => FALSE,
    ])->save();

    $this->drupalGet($this->user->toUrl('edit-form'));
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('Digest subscriptions');
    $this->assertSession()->pageTextContains('There are currently no digests available.');
    $this->assertSession()->pageTextNotContains('My digest');

  }

  /**
   * Tests that users can see and subscribe to digests.
   */
  public function testWidget() {

    /** @var \Drupal\digest\Entity\DigestInterface $digest_one */
    $digest_one = Digest::create([
      'id' => 'digest_one',
      'title' => $this->randomMachineName(),
    ]);
    $digest_one->save();

    /** @var \Drupal\digest\Entity\DigestInterface $digest_two */
    $digest_two = Digest::create([
      'id' => 'digest_two',
      'title' => $this->randomMachineName(),
      'description' => $this->randomString(),
    ]);
    $digest_two->save();

    $this->drupalGet($this->user->toUrl('edit-form'));
    $this->assertSession()->statusCodeEquals(200);

    // Ensure digest details are displayed.
    $this->assertSession()->pageTextContains($digest_one->getTitle());
    $this->assertSession()->pageTextContains($digest_two->getTitle());
    $this->assertSession()->pageTextContains($digest_two->getDescription());

    $digest_one_field_name = 'edit-field-digest-subscriptions-digest-one';
    $digest_two_field_name = 'edit-field-digest-subscriptions-digest-two';

    // Ensure field values are correct.
    $this->assertSession()->checkboxNotChecked($digest_one_field_name);
    $this->assertSession()->checkboxNotChecked($digest_two_field_name);

    // Subscribe to a digest.
    $page = $this->getSession()->getPage();
    $page->checkField($digest_one_field_name);
    $this->submitForm([], 'Save');

    // Ensure default values are present.
    $this->assertSession()->checkboxChecked($digest_one_field_name);
    $this->assertSession()->checkboxNotChecked($digest_two_field_name);

  }

}
